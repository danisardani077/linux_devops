#!/bin/sh
# Cek internet melalui ping
if [[ "$(ping -c 1 google.com | grep '100% packet loss' )" != "" ]]; then
    ip route get 8.8.8.8 | awk '{for(i=1; i<NF; i++){if($i=="src"){i++; print $i; exit}}}'
    
    echo "Internet isn't present"
    exit 1
else
    echo "Internet is present"
    # cek user dengan menggunakan perintah whoami
    if [ "$(whoami)" == "root" ] ; then
        # menjalankan perintah update dan upgrade serta remove  jika memiliki akses root
        apt update && apt upgrade && apt autoremove --purge
    else
        # buka link dengan perintah xdg-open jika user tidak memiliki akses root
        xdg-open "https://www.youtube.com/watch?v=BkvhwRJAYYY"
    fi
fi
